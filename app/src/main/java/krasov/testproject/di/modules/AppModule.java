package krasov.testproject.di.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import krasov.testproject.domain.MainInteractor;
import krasov.testproject.utils.CommonUtils;

/**
 * Created by user on 07.11.2017.
 */

@Module
public class AppModule {
    private Context context;

    public AppModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return context;
    }

    @Provides
    @Singleton
    CommonUtils provideCommonUtils(Context context) {
        return new CommonUtils(context);
    }


    @Provides
    @Singleton
    MainInteractor mainInteractor() {
        return new MainInteractor();
    }
}
