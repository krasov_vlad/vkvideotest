package krasov.testproject.di;

import javax.inject.Singleton;

import dagger.Component;
import krasov.testproject.di.modules.AppModule;
import krasov.testproject.mvp.presenter.MainPresenter;
import krasov.testproject.ui.adapters.VideoListAdapter;

/**
 * Created by user on 07.11.2017.
 */


@Component(modules = {AppModule.class})
@Singleton
public interface AppComponent {
    void inject(MainPresenter mainPresenter);

    void inject(VideoListAdapter videoListAdapter);
}
