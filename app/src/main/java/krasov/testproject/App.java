package krasov.testproject;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;
import com.vk.sdk.VKSdk;

import krasov.testproject.di.AppComponent;
import krasov.testproject.di.DaggerAppComponent;
import krasov.testproject.di.modules.AppModule;

/**
 * Created by user on 07.11.2017.
 */

public class App extends Application {
    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = buildAppComponent();
        VKSdk.initialize(this);
//        if (LeakCanary.isInAnalyzerProcess(this)) {
//             This process is dedicated to LeakCanary for heap analysis.
//             You should not init your app in this process.
//            return;
//        }
//        LeakCanary.install(this);

    }

    private AppComponent buildAppComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
