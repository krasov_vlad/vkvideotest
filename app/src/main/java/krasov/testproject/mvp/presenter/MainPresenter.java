package krasov.testproject.mvp.presenter;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiVideo;
import com.vk.sdk.api.model.VkVideoArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import krasov.testproject.App;
import krasov.testproject.domain.MainInteractor;
import krasov.testproject.mvp.view.MainView;
import krasov.testproject.utils.CommonUtils;
import krasov.testproject.utils.Constants;

/**
 * Created by user on 07.11.2017.
 */

@InjectViewState
public class MainPresenter extends BasePresenter<MainView> {
    private static final String TAG = Constants.TAG_PREFIX + "MainPresenter";


    @Inject
    CommonUtils utils;
    @Inject
    MainInteractor mainInteractor;


    public MainPresenter() {
        App.getAppComponent().inject(this);
    }

    @Override
    public void attachView(MainView view) {
        super.attachView(view);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        if (VKAccessToken.currentToken() != null) {
            makeRequest();
        } else {
            getViewState().startLogin();
        }
    }


    private void makeRequest() {
        mainInteractor.getVideoByUserId(66748).executeSyncWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                VkVideoArray vkApiVideos = new VkVideoArray();
                try {
                    vkApiVideos.parse(response.json);
                    getViewState().initVideoRecyclerView(vkApiVideos);
                } catch (JSONException e) {
                    e.printStackTrace();
                    getViewState().showToast("Ошибка парсинга ответа ВК");
                }
            }
        });
    }

    @Override
    public void detachView(MainView view) {
        super.detachView(view);
    }

    public void loginSuccess(VKAccessToken res) {
        if (res != null) {
            makeRequest();
        }
    }

    public void loginError(VKError error) {
        getViewState().showToast(error.toString());
    }

    public void logoutClick() {
        VKSdk.logout();
        getViewState().startLogin();
    }
}
