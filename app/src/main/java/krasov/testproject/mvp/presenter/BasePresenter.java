package krasov.testproject.mvp.presenter;

import com.arellomobile.mvp.MvpPresenter;

import krasov.testproject.mvp.view.BaseView;

/**
 * Created by user on 07.11.2017.
 */

public class BasePresenter<View extends BaseView> extends MvpPresenter<View> {

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
