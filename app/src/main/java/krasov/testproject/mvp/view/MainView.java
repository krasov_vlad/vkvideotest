package krasov.testproject.mvp.view;

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.vk.sdk.api.model.VkVideoArray;

/**
 * Created by user on 07.11.2017.
 */

public interface MainView extends BaseView {
    @StateStrategyType(OneExecutionStateStrategy.class)
    void startLogin();

    @StateStrategyType(SingleStateStrategy.class)
    void initVideoRecyclerView(VkVideoArray vkVideoArray);

}
