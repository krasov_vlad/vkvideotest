package krasov.testproject.mvp.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * Created by user on 07.11.2017.
 */

public interface BaseView extends MvpView {

    @StateStrategyType(SkipStrategy.class)
    void showToast(String text);

}
