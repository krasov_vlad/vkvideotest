package krasov.testproject.domain;

import android.util.Log;

import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiVideo;
import com.vk.sdk.api.model.VkVideoArray;

import org.json.JSONException;

import krasov.testproject.utils.Constants;

/**
 * Created by user on 09.11.2017.
 */

public class MainInteractor {
    private static final String TAG = Constants.TAG_PREFIX + "MainInteractor";

    public VKRequest getVideoByUserId(int userId) {
        return new VKRequest("video.get", VKParameters.from(VKApiConst.OWNER_ID, userId));

    }
}
