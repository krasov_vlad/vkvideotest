package krasov.testproject.ui.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vk.sdk.api.model.VkVideoArray;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import krasov.testproject.App;
import krasov.testproject.R;
import krasov.testproject.ui.activity.MainActivity;

/**
 * Created by user on 09.11.2017.
 */

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.VHolder> {
    @Inject
    Context context;

    private VkVideoArray vkApiVideos;
    private MainActivity.OnItemClick onItemClick;

    public VideoListAdapter(VkVideoArray vkApiVideos, MainActivity.OnItemClick onItemClick) {
        this.vkApiVideos = vkApiVideos;
        this.onItemClick = onItemClick;
        App.getAppComponent().inject(this);
    }

    @Override
    public VHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new VHolder(v);
    }

    @Override
    public void onBindViewHolder(VHolder holder, int position) {
        holder.textView.setText(vkApiVideos.get(position).title);
        Picasso.with(context)
                .load(vkApiVideos.get(position).photo_320)
                .into(holder.imageView);

        holder.cardView.setOnClickListener(view -> onItemClick.onItemClick(vkApiVideos.get(position)));
    }

    @Override
    public int getItemCount() {
        return vkApiVideos.size();
    }

    public class VHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView imageView;
        @BindView(R.id.title)
        TextView textView;
        @BindView(R.id.cardView)
        CardView cardView;

        public VHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
