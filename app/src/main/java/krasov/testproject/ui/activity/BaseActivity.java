package krasov.testproject.ui.activity;

import android.annotation.SuppressLint;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;

import krasov.testproject.mvp.view.BaseView;

/**
 * Created by user on 07.11.2017.
 */

@SuppressLint("Registered")
public class BaseActivity extends MvpAppCompatActivity implements BaseView {

    @Override
    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }


}
