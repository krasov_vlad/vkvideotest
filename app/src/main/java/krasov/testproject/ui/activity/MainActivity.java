package krasov.testproject.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.model.VKApiVideo;
import com.vk.sdk.api.model.VkVideoArray;

import butterknife.BindView;
import butterknife.ButterKnife;
import krasov.testproject.R;
import krasov.testproject.mvp.presenter.MainPresenter;
import krasov.testproject.mvp.view.MainView;
import krasov.testproject.ui.adapters.VideoListAdapter;
import krasov.testproject.utils.Constants;

public class MainActivity extends BaseActivity implements MainView {
    private static final String TAG = Constants.TAG_PREFIX + "MainActivity";
    @InjectPresenter
    MainPresenter mainPresenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void startLogin() {
        VKSdk.login(this, Constants.sMyScope);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                mainPresenter.loginSuccess(res);
            }

            @Override
            public void onError(VKError error) {
                mainPresenter.loginError(error);
            }
        }))
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void initVideoRecyclerView(VkVideoArray vkVideoArray) {
        recyclerView.setAdapter(new VideoListAdapter(vkVideoArray, vkApiVideo -> {
            Intent i = new Intent(this, VideoActivity.class);
            i.putExtra(Constants.VIDEO, vkApiVideo);
            startActivity(i);
        }));
    }


    public interface OnItemClick {
        void onItemClick(VKApiVideo vkApiVideo);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            mainPresenter.logoutClick();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
