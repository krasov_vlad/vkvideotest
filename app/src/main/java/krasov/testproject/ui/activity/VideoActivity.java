package krasov.testproject.ui.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import com.vk.sdk.api.model.VKApiVideo;

import butterknife.BindView;
import butterknife.ButterKnife;
import krasov.testproject.R;
import krasov.testproject.utils.Constants;

//todo MVP activity......
public class VideoActivity extends AppCompatActivity {


    @BindView(R.id.webView)
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        ButterKnife.bind(this);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        VKApiVideo vkApiVideo = getIntent().getParcelableExtra(Constants.VIDEO);

        webView.loadUrl(vkApiVideo.player);

    }

}
