package krasov.testproject.utils;

import com.vk.sdk.VKScope;

/**
 * Created by user on 15.08.2017.
 */

public interface Constants {
    String TAG_PREFIX = "MYTAG_";
    String[] sMyScope = new String[]{
            VKScope.FRIENDS,
            VKScope.VIDEO,
            VKScope.WALL
    };
    String VIDEO = "videointent";
}
